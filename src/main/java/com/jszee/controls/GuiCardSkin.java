package com.jszee.controls;

import com.jszee.server.standard52CardDeck.Card;
import com.sun.javafx.scene.control.behavior.BehaviorBase;
import com.sun.javafx.scene.control.skin.LabeledSkinBase;
import javafx.beans.InvalidationListener;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.layout.*;

import java.util.Collections;

/**
 * Created by jovaughnlockridge1 on 10/8/16.
 */
public class GuiCardSkin extends LabeledSkinBase<GuiCard, BehaviorBase<GuiCard>> {

    private Card card;
    private InvalidationListener updateCardListener = observable -> updateGuiCard();
    private boolean invalidCard = true;


    public GuiCardSkin(GuiCard control){

        super(control, new BehaviorBase<>(control, Collections.emptyList()));

        getSkinnable().cardObservableValue().addListener(updateCardListener);
        // Labels do not block the mouse by default, unlike most other UI Controls.
        consumeMouseEvents(false);

        registerChangeListener(control.labelForProperty(), "LABEL_FOR");
        getSkinnable().setOnMouseClicked(e -> getSkinnable().fireEvent(new ActionEvent()));
    }

    public void updateGuiCard(){
        this.card = getSkinnable().cardObservableValue().get();
        if(card!=null) {
            Image image = new Image("/images/" + card.getFilename());
            getSkinnable().setBackground(new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
                    BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
            getSkinnable().requestLayout();
        }
    }

    @Override
    protected void layoutChildren(double contentX, double contentY, double contentWidth, double contentHeight){
        if( invalidCard ){
            updateGuiCard();
            invalidCard = false;
        }
      }
}
