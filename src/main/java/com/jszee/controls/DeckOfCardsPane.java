package com.jszee.controls;

import com.jszee.server.standard52CardDeck.Card;
import javafx.animation.*;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jovaughnlockridge1 on 10/9/16.
 */
public class DeckOfCardsPane extends Pane {

    private static final Logger logger = LoggerFactory.getLogger(DeckOfCardsPane.class);
    private List<GuiCard> guiCards = new ArrayList<>();
    public DeckOfCardsPane() {
        super();
    }

    public void init(){
        int x = 0;
        while(x<52) {
            GuiCard card = new GuiCard();
            card.setCard(new Card(null, x, new String("bluecard"), null));
            this.getChildren().add(card);
            guiCards.add(card);
            x++;
        }

        logger.info("{} Cards added to deck ", x);
    }


    public void dealToPlayerOne(){

        final GuiCard card = guiCards.remove(guiCards.size()-1);
        RotateTransition rt = new RotateTransition(Duration.millis(50), card);
        rt.setByAngle(360);
        rt.setCycleCount(4);

        Timeline timeLine = new Timeline();
        timeLine.setCycleCount(1);
        KeyValue kv =  new KeyValue(card.translateYProperty(), 450);
        KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
        timeLine.getKeyFrames().add(kf);

        ParallelTransition pt =  new ParallelTransition();
        pt.getChildren().addAll(rt, timeLine);
        pt.play();
        pt.setOnFinished( l -> this.getChildren().remove(card));
    }


    public void dealToPlayerTwo(){

        final GuiCard card = guiCards.remove(guiCards.size()-1);

        RotateTransition rt = new RotateTransition(Duration.millis(50), card);
        rt.setByAngle(360);
        rt.setCycleCount(4);

        Timeline timeLine = new Timeline();
        timeLine.setCycleCount(1);
        KeyValue kv =  new KeyValue(card.translateXProperty(), -450);
        KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
        timeLine.getKeyFrames().add(kf);


        ParallelTransition pt =  new ParallelTransition();
        pt.getChildren().addAll(rt, timeLine);
        pt.play();

        pt.setOnFinished( l -> this.getChildren().remove(card));
    }

    public void dealToPlayerThree(){

        final GuiCard card = guiCards.remove(guiCards.size()-1);

        RotateTransition rt = new RotateTransition(Duration.millis(50), card);
        rt.setByAngle(360);
        rt.setCycleCount(4);

        Timeline timeLine = new Timeline();
        timeLine.setCycleCount(1);
        KeyValue kv =  new KeyValue(card.translateYProperty(), -450);
        KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
        timeLine.getKeyFrames().add(kf);

        ParallelTransition pt =  new ParallelTransition();
        pt.getChildren().addAll(rt, timeLine);
        pt.play();
        pt.setOnFinished( l -> this.getChildren().remove(card));
    }


    public void dealToPlayerFour(){

        final GuiCard card = guiCards.remove(guiCards.size()-1);

        RotateTransition rt = new RotateTransition(Duration.millis(50), card);
        rt.setByAngle(360);
        rt.setCycleCount(4);

        Timeline timeLine = new Timeline();
        timeLine.setCycleCount(1);
        KeyValue kv =  new KeyValue(card.translateXProperty(), 450);
        KeyFrame kf = new KeyFrame(Duration.millis(300), kv);
        timeLine.getKeyFrames().add(kf);

        ParallelTransition pt =  new ParallelTransition();
        pt.getChildren().addAll(rt, timeLine);
        pt.play();

        pt.setOnFinished( l -> this.getChildren().remove(card));
    }


    @Override
    protected void layoutChildren() {
        List<Node> sortedChildren = new ArrayList<>(getChildren());
        double currentX = 200;
        double currentY = 120;
        for(Node c : sortedChildren) {
            layoutInArea(c, currentX , currentY, 200, 800, 0, HPos.CENTER, VPos.BASELINE );
            currentX+=1.5;
            currentY-=1.5;
        }
    }
}
