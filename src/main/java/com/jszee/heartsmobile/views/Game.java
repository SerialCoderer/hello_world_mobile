package com.jszee.heartsmobile.views;

import com.gluonhq.charm.glisten.mvc.View;
import com.jszee.IScreensController;
import javafx.scene.Parent;

import java.io.IOException;

import static com.jszee.heartsmobile.HeartsPlayer.HEARTS_GAME_SCREEN;

/**
 * Created by jovaughnlockridge1 on 12/30/16.
 */
public class Game {

    private final String name;
    private IScreensController screensController;

    public Game(String name, IScreensController sc){
        this.name = name;
        this.screensController = sc;
    }


    public View getView() {
        try {
            Parent loadScreen = screensController.getScreen(HEARTS_GAME_SCREEN);
            View view = (View) loadScreen;
            view.setName(name);
            return view;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IOException: " + e);
            return new View(name);
        }
    }
}
