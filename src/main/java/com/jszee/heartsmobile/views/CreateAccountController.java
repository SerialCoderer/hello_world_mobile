package com.jszee.heartsmobile.views;

import com.gluonhq.charm.glisten.mvc.View;
import com.google.inject.Inject;
import com.jszee.ControlledScreen;
import com.jszee.IScreensController;
import com.jszee.model.HeartsGameSession;
import com.jszee.model.LoginViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * Created by jovaughnlockridge1 on 8/1/16.
 */
public class CreateAccountController implements ControlledScreen {

    @FXML
    private TextField userNameTextField;

    @FXML
    private TextField passwordTextField;


    @FXML
    private Text actiontarget;

    @FXML
    private Button connectButton;

    private IScreensController controller;

    private LoginViewModel loginViewModel;

    @Inject
    private HeartsGameSession jova;

    @FXML
    private View primary;


    public void initialize() {

        jova.start();
        loginViewModel = new LoginViewModel(jova);
        userNameTextField.textProperty().bindBidirectional(loginViewModel.userNameProperty());
        passwordTextField.textProperty().bindBidirectional(loginViewModel.passwordProperty());


       // actiontarget.textProperty().bind(loginViewModel.isLoggedIn());

//        connectButton.disableProperty().bind(loginViewModel.disableLoginButton().not());
//        connectButton.setOnAction(v -> loginViewModel.connect());
    }


    @Override
    public void setScreenParent(IScreensController screenPage) {
        this.controller = screenPage;
        loginViewModel.setScreenParent(screenPage);
    }



}
