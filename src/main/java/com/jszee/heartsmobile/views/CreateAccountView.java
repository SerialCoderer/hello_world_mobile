package com.jszee.heartsmobile.views;

import com.gluonhq.charm.glisten.mvc.View;
import com.jszee.IScreensController;
import javafx.scene.Parent;

import java.io.IOException;

import static com.jszee.heartsmobile.HeartsPlayer.CREATE_ACCOUNT_SCREEN;
import static com.jszee.heartsmobile.HeartsPlayer.LOGIN_SCREEN;

public class CreateAccountView {

    private final String name;
    private IScreensController screensController;

    public CreateAccountView(String name, IScreensController sc) {
        this.name = name;
        this.screensController = sc;
    }
    
    public View getView() {
        try {
            Parent loadScreen = screensController.getScreen(CREATE_ACCOUNT_SCREEN);
            View view = (View) loadScreen;
            view.setName(name);
            return view;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IOException: " + e);
            return new View(name);
        }
    }
}
