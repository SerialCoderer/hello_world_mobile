package com.jszee.heartsmobile.views;

import com.google.inject.Inject;
import com.jszee.heartsgame.HeartsGame;
import com.jszee.heartsgame.HeartsGameMoveType;
import com.jszee.heartsgame.States.HeartsGameState;
import com.jszee.client.Client;
import com.jszee.client.GameUpdateEvent;
import com.jszee.client.IPlayer;
import com.jszee.server.Move;
import com.jszee.server.serverRequest.ServerActions;
import com.jszee.server.standard52CardDeck.Card;
import com.jszee.server.standard52CardDeck.Suits;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * Created by jovaughnlockridge1 on 8/12/16.
 */


public class GameViewModel{

    private static final Logger logger = LoggerFactory.getLogger(GameViewModel.class);
    private final Client gameSession;
    private SimpleObjectProperty<List<Card>> cardBinder = new SimpleObjectProperty<>();
    private SimpleObjectProperty<List<Card>> deal = new SimpleObjectProperty<>();
    private SimpleObjectProperty<List<Card>> guiPlayingHandCardRemover = new SimpleObjectProperty<>();
    private SimpleObjectProperty<List<Card>> playingTableCardBinder = new SimpleObjectProperty<>();
    private SimpleObjectProperty<List<Card>>  removedPlayingTableCardBinder = new SimpleObjectProperty<> ();

    private StringProperty playerName = new SimpleStringProperty();
    private StringProperty status = new SimpleStringProperty();
    private Lock screenLock = new ReentrantLock();
    private HeartsGameMoveType moveType;


    private String userName;
    private List<Card> playingHand = new ArrayList<>();
    private List<Card> table = new ArrayList<>(4);
    private List<Card> listCards = new ArrayList<>();
    private HeartsGameState currentGameState = null;


    @Inject
    public GameViewModel(IPlayer session){
        session.addGameActionListener(l -> gameUpdate(l));
        this.gameSession = (Client) session;
    }


    private void gameUpdate(GameUpdateEvent<?> event) {

        ServerActions gameAction = event.getAction();

        switch(gameAction){

            case JOIN_GAME_SUCCESSFUL:
                logger.info("JOIN GAME SUCCESSFUL");
                break;

            case LOGIN_SUCCESSFUL:
                this.userName = this.gameSession.getUserName();
                playerName.set(this.userName);
                logger.info("Logging successful for username {}",  this.userName);
                break;

            case MESSAGE:
                this.status.setValue((String) event.getData());
                break;

            case START:
                break;

            case GAME_STATUS_CHANGE:
                screenLock.lock();
                try {
                    HeartsGame heartsGame = (HeartsGame) event.getData();
                    logger.info("Game state has changed to {} ", heartsGame.getGameState());
                    this.currentGameState = heartsGame.getGameState();
                    if(heartsGame.getGameState().getClass() == heartsGame.DEALING.getClass()) {


                            logger.info("Updating Screen with event {} ", event);
                            clearPlayingTable();
                            clearHand();
                            HeartsGame board = (HeartsGame) event.getData();
                            moveType = board.getGameState().getHeartGameMoveType();
                            Collection<Card<Suits, Integer>> playedCards = board.getPlayerMoves().values();

                            List<Card<Suits, Integer>> playHandCards = board.getPlayer(gameSession.getUserName()).getPlayerCards();

                            Collections.sort(playHandCards, new Comparator<Card<Suits, Integer>>() {
                                @Override
                                public int compare(Card<Suits, Integer> o1, Card<Suits, Integer> o2) {
                                    if (o1.getSuit() == o2.getSuit()) {
                                        if (o1.getOrdinal() > o2.getOrdinal()) {
                                            return 1;
                                        } else {
                                            return -1;
                                        }
                                    } else if (o1.getSuit() == Suits.HEARTS && o2.getSuit() != Suits.HEARTS) {
                                        return 1;
                                    } else if (o1.getSuit() != Suits.HEARTS && o2.getSuit() == Suits.HEARTS) {
                                        return -1;
                                    } else if (o1.getSuit() == Suits.CLUBS &&
                                            (o2.getSuit() == Suits.DIAMONDS ||
                                                    o2.getSuit() == Suits.SPADES)) {
                                        return 1;
                                    } else if ((o1.getSuit() == Suits.DIAMONDS || o1.getSuit() == Suits.SPADES) && o2.getSuit() == Suits.CLUBS) {
                                        return -1;
                                    } else if (o1.getSuit() == Suits.SPADES && o2.getSuit() == Suits.DIAMONDS) {
                                        return 1;
                                    } else if (o1.getSuit() != Suits.DIAMONDS && o2.getSuit() == Suits.SPADES) {
                                        return -1;
                                    }
                                    return -1;
                                }
                            });
                            table.addAll(playedCards);

                            playingHand.addAll(playHandCards);
                            logger.info("Cards left in hand {} ", playingHand);
                            dealCards(playingHand);
                            playingTableCardBinder.set(null);
                            playingTableCardBinder.set(table);
                        }
                    } finally {
                        screenLock.unlock();
                    }
            break;


            case UPDATE_SCREEN:
                screenLock.lock();

                try{
                    logger.info("Updating Screen with event {} ", event);
                    clearPlayingTable();
                    clearHand();
                    HeartsGame board = (HeartsGame) event.getData();
                    moveType = board.getGameState().getHeartGameMoveType();
                    Collection<Card<Suits, Integer>> playedCards = board.getPlayerMoves().values();

                    List<Card<Suits, Integer>> playHandCards = board.getPlayer(gameSession.getUserName()).getPlayerCards();

                    Collections.sort(playHandCards, new Comparator<Card<Suits, Integer>>(){
                        @Override
                        public int compare(Card<Suits, Integer> o1, Card<Suits, Integer> o2) {
                            if(o1.getSuit()==o2.getSuit()){
                                if(o1.getOrdinal()>o2.getOrdinal()) {
                                    return 1;
                                }else{
                                    return -1;
                                }
                            }else if(o1.getSuit() == Suits.HEARTS &&  o2.getSuit()!= Suits.HEARTS){
                                return 1;
                            }else if( o1.getSuit()!= Suits.HEARTS && o2.getSuit()== Suits.HEARTS){
                                return -1;
                            }else if(o1.getSuit() == Suits.CLUBS &&
                                    (o2.getSuit()== Suits.DIAMONDS ||
                                            o2.getSuit()== Suits.SPADES)){
                                return 1;
                            }else if((o1.getSuit() == Suits.DIAMONDS || o1.getSuit() == Suits.SPADES) && o2.getSuit()== Suits.CLUBS){
                                return -1;
                            }else if(o1.getSuit() == Suits.SPADES  && o2.getSuit()== Suits.DIAMONDS){
                                return 1;
                            }else if(o1.getSuit()!= Suits.DIAMONDS && o2.getSuit() == Suits.SPADES){
                                return -1;
                            }
                            return -1;
                        }
                    });
                    table.addAll(playedCards);

                    playingHand.addAll(playHandCards);
                    logger.info("Cards left in hand {} ", playingHand);
                    addCardToScreen(playingHand);
                    playingTableCardBinder.set(null);
                    playingTableCardBinder.set(table);

                }finally{
                    screenLock.unlock();
                }
                break;
        }
    }


    public SimpleObjectProperty<List<Card>> getCardBinder(){
        return cardBinder;
    }

    public SimpleObjectProperty<List<Card>> deal(){
        return deal;
    }


    public SimpleObjectProperty<List<Card>> getGuiPlayingHandCardRemover(){
        return guiPlayingHandCardRemover;
    }

    public StringProperty getStatusBar(){ return status;}

    public StringProperty getPlayerName(){ return playerName; }


    /**
     * This is used to create a game move. You will either
     * add card a to your move or remove the card from your
     * move.
     *
     * @param card the card added to your move.
     * @param addingCard determines if the card is added to the move or removed if present.
     */
    public void addCardToMove(Card card, boolean addingCard){
        screenLock.lock();
        try{
            if(addingCard) {
                playingHand.remove(card);
                listCards.add(card);
                logger.info("Attempting to send card {} ", card);
            }else{
                playingHand.add(card);
                listCards.remove(card);
                logger.info("Removing attempt to send card {} ", card);
            }
        }finally{
            screenLock.unlock();
        }
    }

    public void sendMove(){
        if(listCards==null || listCards.isEmpty()){
            return;
        }
        screenLock.lock();
        List<Card> copyOfCardsMove = new ArrayList<>();
        try{
            for(Card c : listCards){
                copyOfCardsMove.add(c);
            }
//            copyOfCardsMove = listCards.stream().collect(Collectors.toList());
            listCards.clear();
            removeCardFromPlayerHandOnGui(copyOfCardsMove);

        }finally{
            screenLock.unlock();
        }
        move(new Move(moveType,  copyOfCardsMove));
    }

    private void move(Move move){
        screenLock.lock();
        try{
            gameSession.move(move);
        }finally{
            screenLock.unlock();
        }
    }

    private void clearHand(){
        screenLock.lock();
        try{
            List<Card> copyOfCardsInHand = new ArrayList<>();

            for(Card c: playingHand){
                copyOfCardsInHand.add(c);
            }
            //playingHand.stream().collect(Collectors.toList());
            removeCardFromPlayerHandOnGui(null);
            removeCardFromPlayerHandOnGui(copyOfCardsInHand);
            playingHand.clear();
        }finally{
            screenLock.unlock();
        }
    }

    private void removeCardFromPlayerHandOnGui(List<Card> cards){
        guiPlayingHandCardRemover.set(cards);
    }

    private void addCardToScreen(List<Card> cards){
        cardBinder.set(null);
        cardBinder.set(cards);
    }

    private void dealCards(List<Card> cards){
        deal.set(null);
        deal.set(cards);
    }

    private void clearPlayingTable(){
        removedPlayingTableCardBinder.set(null);
        if(!table.isEmpty()) {
            List<Card> copyOfCardsOnTable = new ArrayList<>();

            for(Card c : table){
                copyOfCardsOnTable.add(c);
            }
            //table.stream().collect(Collectors.toList());
            removedPlayingTableCardBinder.set(copyOfCardsOnTable);
            table.clear();
        }
    }


    public  SimpleObjectProperty<List<Card>>  getRemovedPlayingTableCardBinder(){
        return removedPlayingTableCardBinder;
    }

    public SimpleObjectProperty<List<Card>>  getPlayingTableCardBinder(){
        return playingTableCardBinder;
    }
}
