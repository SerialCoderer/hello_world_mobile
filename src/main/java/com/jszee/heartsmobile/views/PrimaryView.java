package com.jszee.heartsmobile.views;

import com.gluonhq.charm.glisten.mvc.View;
import java.io.IOException;

import com.jszee.IScreensController;
import javafx.scene.Parent;

import static com.jszee.heartsmobile.HeartsPlayer.LOGIN_SCREEN;

public class PrimaryView {

    private final String name;
    private IScreensController screensController;

    public PrimaryView(String name, IScreensController sc) {
        this.name = name;
        this.screensController = sc;
    }
    
    public View getView() {
        try {
            Parent loadScreen = screensController.getScreen(LOGIN_SCREEN);
            View view = (View) loadScreen;
            view.setName(name);
            return view;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IOException: " + e);
            return new View(name);
        }
    }
}
