package com.jszee.heartsmobile;


import com.gluonhq.charm.glisten.application.MobileApplication;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.jszee.GameModule;
import com.jszee.ScreensController;
import com.jszee.heartsmobile.views.CreateAccountView;
import com.jszee.heartsmobile.views.Game;
import com.jszee.heartsmobile.views.PrimaryView;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HeartsPlayer extends MobileApplication {


    private static final Logger logger = LoggerFactory.getLogger(HeartsPlayer.class);

    private Injector injector = Guice.createInjector(new GameModule());

    public static final String PRIMARY_VIEW = HOME_VIEW;
    public static final String MENU_LAYER = "Side Menu";
    public static final String LOGIN_SCREEN = "login";
    public static final String LOGIN_SCREEN_FXML = "/com/jszee/heartsmobile/views/login.fxml";

    public static final String HEARTS_GAME_SCREEN = "hearts";
    public static final String HEARTS_GAME_SCREEN_FXML = "/com/jszee/heartsmobile/views/heartsView.fxml";

    public static final String CREATE_ACCOUNT_SCREEN = "create_account";
    public static final String CREATE_ACCOUNT_FXML = "/com/jszee/heartsmobile/views/createAccount.fxml";

    private ScreensController mainContainer = null;

    @Override
    public void init() {

        try {


            mainContainer = new ScreensController(injector);

            addViewFactory(PRIMARY_VIEW, () -> new PrimaryView(PRIMARY_VIEW, mainContainer).getView());
            addViewFactory(CREATE_ACCOUNT_SCREEN, () -> new CreateAccountView(CREATE_ACCOUNT_SCREEN, mainContainer).getView());

            mainContainer.loadScreen(LOGIN_SCREEN, LOGIN_SCREEN_FXML);
            mainContainer.loadScreen(CREATE_ACCOUNT_SCREEN, CREATE_ACCOUNT_FXML);
            mainContainer.loadScreen(HEARTS_GAME_SCREEN, HEARTS_GAME_SCREEN_FXML);
            mainContainer.setScreen(LOGIN_SCREEN);


            addViewFactory(HEARTS_GAME_SCREEN, () -> new Game(HEARTS_GAME_SCREEN, mainContainer).getView());

        }catch(IOException ioe){
            logger.error("issue with opening a file");
            ioe.printStackTrace();
        }

    }


    @Override
    public void postInit(Scene scene) {
//        Swatch.BLUE.assignTo(scene);

        scene.getStylesheets().add(HeartsPlayer.class.getResource("style.css").toExternalForm());
        ((Stage) scene.getWindow()).getIcons().add(new Image(HeartsPlayer.class.getResourceAsStream("/icon.png")));
        DrawerManager drawerManager = new DrawerManager();
        mainContainer.setDrawerManager(drawerManager);

    }
}
