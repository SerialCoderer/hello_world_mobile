package com.jszee;

import javafx.scene.Parent;

import java.io.IOException;

/**
 * Created by jovaughnlockridge1 on 11/20/16.
 */
public interface IScreensController {

    void loadScreen(String id, String resource) throws IOException;
    Parent getScreen(String id) throws IOException;
    boolean setScreen(final String name);

}
