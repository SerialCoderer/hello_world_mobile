package com.jszee.model;

import com.jszee.ControlledScreen;
import com.jszee.IScreensController;
import com.jszee.heartsgame.HeartsGame;
import com.jszee.heartsmobile.HeartsPlayer;
import com.jszee.client.GameActionListener;
import com.jszee.client.GameUpdateEvent;
import javafx.beans.property.*;

/**
 * Created by jovaughnlockridge1 on 7/30/16.
 */
public class LoginViewModel implements GameActionListener, ControlledScreen {

    private final StringProperty userName = new SimpleStringProperty();
    private final StringProperty password = new SimpleStringProperty();
    private final ReadOnlyBooleanWrapper loginPossible = new ReadOnlyBooleanWrapper();
    private final ReadOnlyBooleanWrapper loggedIn = new ReadOnlyBooleanWrapper();
    private boolean isLoggedIn;

    private IScreensController controller;


    private HeartsGameSession sess;


    public LoginViewModel(HeartsGameSession session){
        this.sess = session;
        loginPossible.bind(userName.isNotEmpty().and(password.isNotEmpty()));
    }

    public StringProperty userNameProperty() {
        return userName;
    }
    public StringProperty passwordProperty() {
        return password;
    }
    public ReadOnlyBooleanProperty isLoginPossibleProperty() {
        return loginPossible.getReadOnlyProperty();
    }

//    public SimpleStringProperty isLoggedIn() {
//        return labelText;
//    }
//
    public BooleanProperty disableLoginButton(){
        return loggedIn;
    }

    public void login(){
        isLoggedIn = sess.connect(userName.get(), password.getValue());

        loggedIn.set(true);
//        if(isLoggedIn){
//            labelText.set("Logged In");
//        }else{
//            labelText.set("Not Logged In");
//        }
    }

    public void connect(){
        sess.joinGame(new HeartsGame());
        controller.setScreen(HeartsPlayer.HEARTS_GAME_SCREEN);
    }

    @Override
    public void updateGameScreen(GameUpdateEvent event) {
        System.out.println("Screen event "+event);
    }

    @Override
    public void setScreenParent(IScreensController screenPage) {
        this.controller = screenPage;
    }

}
