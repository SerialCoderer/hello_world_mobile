package com.jszee.model;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jszee.client.Client;
import com.jszee.client.GameActionListener;
import com.jszee.client.GameUpdateEvent;
import com.jszee.server.ISocket;
import com.jszee.server.serverRequest.ServerActions;
import com.jszee.server.service.spi.BoardGame;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jovaughnlockridge1 on 7/31/16.
 */
@Singleton
public class HeartsGameSession extends Client<BoardGame> {


    private List<GameActionListener> listeners = new LinkedList<>();

    @Inject
    public HeartsGameSession(ISocket connection){
        super(connection);
    }

    @Override
    public void updateBoard(BoardGame game) {
        //listeners.stream().forEach(l -> l.updateGameScreen(new GameUpdateEvent<BoardGame<HeartsGameMoveType, HeartsGamePlayer>>(game,  ServerActions.UPDATE_BOARD)));
    }

    @Override
    public void updateScore(BoardGame score) {
       // listeners.stream().forEach(l -> l.updateGameScreen(new GameUpdateEvent<BoardGame<HeartsGameMoveType, HeartsGamePlayer>>(score,  ServerActions.UPDATE_SCORE)));
    }

    @Override
    public void updateScreen(ServerActions action, BoardGame screen) {
      //  listeners.stream().forEach(l -> l.updateGameScreen(new GameUpdateEvent<BoardGame<HeartsGameMoveType, HeartsGamePlayer>>(screen,  action)));
    }

    @Override
    public void message(String message) {
        //listeners.stream().forEach(l -> l.updateGameScreen(new GameUpdateEvent<String>(message, ServerActions.MESSAGE)));
    }

    @Override
    public void startGame(BoardGame game) {

        for(GameActionListener l : listeners){

            l.updateGameScreen(new GameUpdateEvent(game, ServerActions.UPDATE_SCREEN));
        }
      //listeners.stream().forEach(l -> l.updateGameScreen(new GameUpdateEvent<BoardGame<HeartsGameMoveType, HeartsGamePlayer>>(game, ServerActions.UPDATE_SCREEN)));
    }

    public void addGameActionListener(GameActionListener l){

        listeners.add(l);
    }

    @Override
    public void successFullLogin(UUID uuid) {

        for(GameActionListener l : listeners){

            l.updateGameScreen(new GameUpdateEvent(uuid, ServerActions.LOGIN_SUCCESSFUL));
        }

        // listeners.stream().forEach(l -> l.updateGameScreen(new GameUpdateEvent(uuid, ServerActions.LOGIN_SUCCESSFUL)));

    }

    @Override
    public void gameStateChange(BoardGame game) {

        for(GameActionListener l : listeners){

            l.updateGameScreen(new GameUpdateEvent(game, ServerActions.GAME_STATUS_CHANGE));
        }


       // listeners.stream().forEach(l -> l.updateGameScreen(new GameUpdateEvent(uuid, ServerActions.GAME_STATUS_CHANGE)));
    }
}
