package com.jszee;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.jszee.model.HeartsGameSession;
import com.jszee.client.IPlayer;
import com.jszee.server.GameHost;
import com.jszee.server.GamePort;
import com.jszee.server.ISocket;
import com.jszee.server.JSocket;

/**
 * Created by jovaughnlockridge1 on 8/31/16.
 */
public class GameModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(HeartsGameSession.class).in(Singleton.class);
        bind(IPlayer.class).to(HeartsGameSession.class);
        bindConstant().annotatedWith(GameHost.class).to("192.168.1.152");
        bindConstant().annotatedWith(GamePort.class).to(new Integer(9001));
        bind(ISocket.class).to(JSocket.class);


    }
}

